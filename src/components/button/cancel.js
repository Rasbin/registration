import React, {Component} from 'react';
import './button.css';

class Cancel extends Component {
    constructor(){
        super();
        this.handleCancel = this.handleCancel.bind(this);
    }
    handleCancel(e){
        e.preventDefault();
        this.props.cancel();
    }
    render(){
        return(
            <input type="submit" onClick={this.handleCancel}className="btn btn-secondary" value="Cancel" />
        );
    }
}

export default Cancel;
