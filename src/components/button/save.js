import React, {Component} from 'react';
import './button.css';

class Save extends Component {

    render(){
        return(
            <input type="submit" className="btn btn-primary" value="Save" />
        );
    }

}

export default Save;
