import React, { Component } from 'react';
import './header.css';

import logo from './logo.jpg';
class Header extends Component{
    render(){
        return(
            <header className="app-header">
                <div className="container">
                    <img src={logo} alt="logo" />
                    <h1 className="title">Nord Software</h1>
                </div>
            </header>
        );
    }
}

export default Header;
