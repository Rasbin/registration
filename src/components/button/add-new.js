import React, {Component} from 'react';
import './button.css';

class AddNew extends Component {

    render(){
        return(
            <input type="submit" className="btn btn-primary btn-sw-primary" value="Add new" />
        );
    }
}

export default AddNew;
