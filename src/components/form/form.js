import React, {Component} from 'react';
import './form.css';

import AddNew from './../button/add-new';
class Form extends Component{

    constructor(){
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit( e ){
        e.preventDefault();
        var participant ={
            name     : this.name.value,
            email    : this.email.value,
            phone_no : this.phoneNo.value
        }
        this.props.add( participant );
        this.name.value ='';
        this.email.value ='';
        this.phoneNo.value ='';
    }

    render(){
        return(
            <form className="form clearfix" onSubmit={this.handleSubmit}>
                <input type="text" className="form-control" ref={(name) => {this.name = name;}} placeholder="Full name" required/>
                <input type="email" className="form-control" ref={(email) => {this.email = email;}} placeholder="E-mail address" />
                <input type="text" className="form-control" ref={(phoneNo) => {this.phoneNo= phoneNo;}} placeholder="Phone number" required/>
                <AddNew />
            </form>
        )
    }

}

export default Form;
