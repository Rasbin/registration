import React, {Component} from 'react';
import './table.css';

import Thead from './../thead/thead';
import Tbody from './../tbody/tbody';

class Table extends Component{

    render(){
        return(
            <table className="participant-list">
                <Thead />
                <Tbody participants={this.props.participants} delete={this.props.delete} update={this.props.update} />
            </table>
        );
    }

}

export default Table;
