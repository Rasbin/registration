This simple registration form is developed as a test to apply at integrify (in co-ordination with NORD SOFTWARE)
for the front end trainee position. After developing in local environment, this task is uploaded in bitbucket at
the final stage for sharing my task.

This task is developed taking help from various online and offline resources.



INSTALLATION GUIDE :
 
To install this app , we need to follow the following steps:

1. Open terminal and Clone repo  
(git clone https://Rasbin@bitbucket.org/Rasbin/registration.git )

2. Install latest stable version of node js
(https://nodejs.org/en/)
[Recommended version : 6.10.2 LTS]

3. Go to registration directory

4. Run the command npm install (This command installs all the dependency packages)

5. Run the command npm install -g json-server for JSON Server.

6. Run the command json-server --watch db.json --port 3004 (Starts watching JSON server at port 3004)

7. In another command prompt (same directory like before) Run the command npm start to start the application.
   You should see the application start running.
   
   

MAJOR RESPONSIBILITIES :

The major task was to develop a simple sign up form and a list of participants with React
with the following requirements :

i. Use create react app to scaffold the application.

ii. Generate 20 participants with random values for the following properties :
    id, name, email address and phone number.

iii. Render a table that displays the participants on individual rows.

iv. Create a form for adding new participants to the table and validate.

v. Make each participant editable by clicking on a table cell. 

vi. Add support for deleting rows.

vii. Make each column sortable upon clicking on a column header.

viii. Write a developer friendly installation guide.

ix. Deploy a live build on the internet.

x. Follow the design as accurately as possible.

[NOTE : Among these, sorting function and deploying a live build on the internet is still to be done. ]


Please feel free to drop me an email at rasbin.rijal@gmail.com if you have any queries related to this app.




