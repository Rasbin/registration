import React, {Component} from 'react';
import './title.css';

class Title extends Component{

    render(){
        return(
            <h2 className="title">List of participants</h2>
        );
    }
}

export default Title;
