import React, { Component } from 'react';
import './thead.css';

class Thead extends Component{

    render(){
        return(
            <thead>
                <tr>
                    <th>Name</th>
                    <th>E-mail address</th>
                    <th>Phone number</th>
                    <th></th>
                </tr>
            </thead>
        );
    }
}

export default Thead;
