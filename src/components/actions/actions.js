import React, { Component } from 'react';
import './action.css';

class Action extends Component{

    constructor(){
        super();
        this.handleDelete = this.handleDelete.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
    }

    handleEdit( e ){
        e.preventDefault();
        this.props.edit();
    }

    handleDelete( e ){
        e.preventDefault();
        this.props.delete( this.props.id, this.props.index );
    }

    render(){
        return(
            <td className="actions">
                <a href="#" onClick={this.handleEdit}><span className="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                <a href="#" onClick={this.handleDelete} ><span className="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
            </td>
        );
    }
}

export default Action;
