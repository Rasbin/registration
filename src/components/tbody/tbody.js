import React, { Component } from 'react';
import './tbody.css';

import Row from './../row/row';

class Tbody extends Component{

    render(){
        var participants = this.props.participants,
            rows         = [];
            participants.forEach( ( post, index ) =>{
                rows.push( <Row key={participants[index].id} participant={participants[index]} delete={this.props.delete} index={index} update={this.props.update}/> );
            });

        return(
            <tbody>
                {rows}
            </tbody>
        );
    }
}

export default Tbody;
