import React, {Component} from 'react';
import './row.css';

import Action from './../actions/actions';
import Save from './../button/save';
import Cancel from './../button/cancel';

class Row extends Component{

    constructor(){
        super();
        this.state ={
            editable: false
        }
        this.renderForm = this.renderForm.bind(this);
        this.renderNormal = this.renderNormal.bind(this);
        this.edit = this.edit.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
    }

    edit(){
        this.setState((prevState, props) => ({
          editable: !prevState.editable
        }));
    }

    handleUpdate(e){
        e.preventDefault();
        var participant ={
            name     : this.name.value,
            email    : this.email.value,
            phone_no : this.phoneNo.value
        }
        this.props.update( this.props.participant.id, this.props.index, participant );
        this.edit();
    }

    renderForm(){
        return(
            <tr>
                <td colSpan="4">
                    <form className="form update clearfix" onSubmit={this.handleUpdate}>
                        <input type="text" className="form-control" defaultValue={this.props.participant.name} ref={(name)=>{this.name = name;}}required />
                        <input type="email" className="form-control" ref={(email)=>{this.email = email;}} defaultValue={this.props.participant.email} required />
                        <input type="text" className="form-control" defaultValue={this.props.participant.phone_no} ref={(phoneNo)=>{this.phoneNo = phoneNo;}}required />
                        <div className="update-actions">
                            <Cancel cancel={this.edit}/>
                            <Save update={this.props.update} index={this.props.index} />
                        </div>
                    </form>
                </td>
            </tr>
        );
    }

    renderNormal(){
        return(
            <tr>
                <td>{this.props.participant.name}</td>
                <td>{this.props.participant.email}</td>
                <td>{this.props.participant.phone_no}</td>
                <Action
                    delete={this.props.delete}
                    edit={this.edit}
                    id={this.props.participant.id}
                    index={this.props.index} />
            </tr>
        );
    }

    render(){
        if(this.state.editable){
            return( this.renderForm() );
        }else{
            return( this.renderNormal() );
        }
    }

}

export default Row;
