import React, { Component } from 'react';
import './App.css';
import jQuery from 'jquery';
import './../node_modules/bootstrap/dist/css/bootstrap.css';

//imported components
import Title from './components/title/title';
import Form from './components/form/form';
import Table from './components/table/table';
import Header from './components/header/header';

class App extends Component {

    constructor( props ){
        super( props );
        this.state ={
            participants:{}
        };
        this.add = this.add.bind(this);
        this.get = this.get.bind(this);
        this.delete = this.delete.bind(this);
        this.update = this.update.bind(this);
        this.baseUrl = '//localhost:3004/';
    }

    componentDidMount(){
        this.get()
    }

    get(){
        var _this = this;
        jQuery.ajax({
            url       : _this.baseUrl+'participants',
            dataType  : 'json',
            type      : 'GET',
            success   : function( data ){
                _this.setState( {participants: data} );
            },
            error     : function(){
                console.error('Error on Fetching data');
            }
        });
    }


    add( data ){
        var _this = this;
        jQuery.ajax({
            url       : _this.baseUrl+'participants',
            data      : data,
            type      : 'POST',
            dataType  : 'json',
            success   : function( data ){
                _this.state.participants.splice(0, 0, data);
                _this.setState( {participants: _this.state.participants } );
            },
            error     : function(){
                console.error('Error on Fetching data');
            }
        });
    }

    delete( id, index ){
        var _this = this;
        jQuery.ajax({
            url       : _this.baseUrl +'participants/'+id,
            dataType  : 'json',
            type      : 'DELETE',
            success   : function( data ){
                _this.state.participants.splice( index, 1 );
                _this.setState( { participants : _this.state.participants } );
            },
            error     : function(){
                console.error('Error on Deleting data');
            }
        });
    }

    update( id, index, data){
        var _this = this;
        jQuery.ajax({
            url       : _this.baseUrl +'participants/'+id,
            data      : data,
            dataType  : 'json',
            type      : 'PUT',
            success   : function( data ){
                _this.state.participants[index] = data;
                _this.setState( { participants : _this.state.participants } );
            },
            error     : function(){
                console.error('Error on Deleting data');
            }
        });
    }

    render() {
        if( this.state.participants.length > 0 ){
            return (
              <div className="App">
                <Header />
                <div className="container">
                    <Title />
                    <Form add={this.add} />
                    <Table participants={this.state.participants} delete={this.delete} update={this.update} />
                </div>
              </div>
            );
        }else{
            return (
                <div className="App">
                <Header />
                <div className="container">
                <Title />
                <Form add={this.add}/>
                <h1 className="no-record">No record</h1>
                </div>
                </div>
            );
        }
    }

}

export default App;
